package generators;

import dto.Store;

import java.util.Date;

public class StoreGenerator {
    private static int COUNT = 0;

    public static Store getNewPet() {
        COUNT++;
        return Store.builder()
                .id(0L)
                .petId(0L)
                .quantity(0)
                .shipDate("2022-05-13T07:16:50.638+0000")
                .status("placed")
                .complete(true)
                .build();

    }



}
