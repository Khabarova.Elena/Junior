package dto;
import lombok.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.Date;
import java.time.Instant;

@Setter
@Getter
@Builder
@AllArgsConstructor
@JsonSerialize
@EqualsAndHashCode
@ToString
@NoArgsConstructor
@Data

public class Store {
    private Long id;
    private Long petId;
    private Integer quantity;
    private String shipDate;
    private String status;
    private Boolean complete;



}

