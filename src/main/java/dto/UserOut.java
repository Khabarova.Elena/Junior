
package dto;

import lombok.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@Getter
@Builder
@AllArgsConstructor
@JsonSerialize
@EqualsAndHashCode
@ToString
@NoArgsConstructor



public class UserOut {


    private Long code;

    private String message;

    private String type;


}
