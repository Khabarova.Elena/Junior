package dto;
import lombok.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.Date;
import java.time.Instant;

@Getter
@Builder
@AllArgsConstructor
@JsonSerialize
@EqualsAndHashCode
@ToString
@NoArgsConstructor


public class StoreOut {
    private Long id;
    private Long petId;
    private Integer quantity;
    private String shipDate;
    private String status;
    private Boolean complete;
    private Long code;
    private String type;
    private String message;






}
