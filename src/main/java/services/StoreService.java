package services;

import com.atlassian.oai.validator.restassured.OpenApiValidationFilter;
import dto.Store;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import static io.restassured.RestAssured.given;

public class StoreService {
    OpenApiValidationFilter filter = new OpenApiValidationFilter("https://petstore.swagger.io/v2/swagger.json");
    private RequestSpecification spec;


    public StoreService() {
        spec = given()
                .baseUri("https://petstore.swagger.io/v2")
                .filter(filter)
                .log().all()
                .contentType(ContentType.JSON);
    }

    public Response PlaceAnOrderForAPet(Store store) {
        return given(spec)
                .body(store)
                .post("/store/order");
    }

    public Response FindPurchaseOrderById(Integer orderId) {

        return given(spec)
                .when()
                .get("/store/order/" + orderId);
    }


    public Response DeletePurchaseOrderById(Integer orderId) {

        return given(spec)
                .when()
                .delete("/store/order/" + orderId);

    }

    public Response ReturnsPetInventoriesByStatus(){

        return given(spec)
                .when()
                .get("/store/inventory/");


    }


}
