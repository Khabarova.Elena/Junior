package services;
import com.atlassian.oai.validator.restassured.OpenApiValidationFilter;
import dto.User;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.util.ArrayList;

import static io.restassured.RestAssured.given;

public class UserService {
    OpenApiValidationFilter filter = new OpenApiValidationFilter("https://petstore.swagger.io/v2/swagger.json");
    private RequestSpecification spec;

    public UserService(){
        spec = given()
                .baseUri("https://petstore.swagger.io/v2")
                .filter(filter)
                .log().all()
                .contentType(ContentType.JSON);

    }

    public Response CreateUser(User user){
        return given(spec)
                .body(user)
                .post("/user");





    }

    public Response GetUserByName(String name){
        return given(spec)
                .when()
                .get("/user/" + name);
    }


    public Response PutUserByName(String name){
        return given(spec)
                .when()
                .put("/user/" + name);

    }

    public Response DeleteUserByName(String name){
        return given()
                .baseUri("https://petstore.swagger.io/v2")
                .filter(filter)
                .log().all()
                .when()
                .delete("/user/" + name);

    }

    public Response GetUserByLogin(String username){
        return given(spec)
                .when()
                .get("user/login" + username);
    }




    public Response GetUserByLogout(){
        return given(spec)
                .when()
                .get("user/logout");
    }





    public Response CreateArrayUsers(User[] user) {
        return given(spec)
                .body(user)
                .post("/user/createWithArray");

    }

    public Response CreateListOfUsers(ArrayList<User> user) {
        return given(spec)
                .body(user)
                .post("/user/createWithList");

    }
}

