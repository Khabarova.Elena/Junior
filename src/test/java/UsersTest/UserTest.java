package UsersTest;

import dto.User;
import dto.UserOut;
import generators.UserGenerator;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import services.UserService;

import java.util.ArrayList;

public class UserTest {
    @Test
    public void createUserTest(){
        User user = UserGenerator.getNewUser();


        UserService userService = new UserService();
        Response response = userService.CreateUser(user);
         UserOut actual = response.then()
                 .log().all()
                 .assertThat()
                 .statusCode(200)
                 .extract()
                 .body()
                 .as(UserOut.class);


         UserOut expected = UserOut.builder()
                 .code(200L)
                 .message(user.getId().toString())
                 .type("unknown")
                 .build();

        Assertions.assertEquals(expected, actual);

    }

    @ParameterizedTest
    @ValueSource(strings = {"admin", "moderator", "user"})
    public void getUserByName(String name){
        UserService userService = new UserService();
        Response response = userService.GetUserByName(name);

        UserOut actual = response.then()
                .log().all()
                .statusCode(404)
                .extract()
                .body()
                .as(UserOut.class);

        UserOut expected = UserOut.builder()
                .code(1L)
                .message("User not found")
                .type("error")
                .build();

        Assertions.assertEquals(expected, actual);
    }


    @Test
    public void createListUserTest(){
        ArrayList<User> users = new ArrayList();
        users.add(UserGenerator.getExistUser());



        UserService userService = new UserService();
        Response response = userService.CreateListOfUsers(users);
        UserOut actual = response.then()
                .log().all()
                .statusCode(200)
                .extract()
                .body()
                .as(UserOut.class);

        UserOut expected = UserOut.builder()
                .code(200L)
                .message("ok")
                .type("unknown")
                .build();

        Assertions.assertEquals(expected, actual);
    }




    @Test
    public void createArrayUsersTest(){
        User[] users = {UserGenerator.getExistUser(), UserGenerator.getExistUser()};

        UserService userService = new UserService();
        Response response = userService.CreateArrayUsers(users);
        UserOut actual = response.then()
                .log().all()
                .statusCode(200)
                .extract()
                .body()
                .as(UserOut.class);

        UserOut expected = UserOut.builder()
                .code(200L)
                .message("ok")
                .type("unknown")
                .build();

        Assertions.assertEquals(expected, actual);
    }



    @ParameterizedTest
    @ValueSource(strings = {"user1"})
    public void putUserByNameTest(String name){
        UserService userService = new UserService();
        Response response = userService.PutUserByName(name);

        UserOut actual = response.then()
                .log().all()
                .statusCode(200)
                .extract()
                .body()
                .as(UserOut.class);

        UserOut expected = UserOut.builder()
                .code(200L)
                .type("unknown")
                .message("6874989595")
                .build();

        Assertions.assertEquals(expected, actual);
    }


    @ParameterizedTest
    @ValueSource(strings = {"user1"})
    public void deleteUserByNameTest(String name){
        UserService userService = new UserService();
        Response response = userService.DeleteUserByName(name);


    }




    @ParameterizedTest
    @ValueSource(strings = {"username"})
    public void getUserByLoginTest(String username){
        UserService userService = new UserService();
        Response response = userService.GetUserByLogin(username);
        UserOut actual = response.then()
                .log().all()
                .statusCode(404)
                .extract()
                .body()
                .as(UserOut.class);

        UserOut expected = UserOut.builder()
                .code(1L)
                .message("User not found")
                .type("error")
                .build();

        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void getUserByLogoutTest(){
        UserService userService = new UserService();
        Response response = userService.GetUserByLogout();

        UserOut actual = response.then()
                .log().all()
                .statusCode(200)
                .extract()
                .body()
                .as(UserOut.class);

        UserOut expected = UserOut.builder()
                .code(200L)
                .message("ok")
                .type("unknown")
                .build();

        Assertions.assertEquals(expected, actual);
    }




}



