package StoreTest;

import dto.Store;
import dto.StoreOut;
import generators.StoreGenerator;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import services.StoreService;




public class StoreTest {
    @Test
    public void placeAnOrderForAPetTest(){
        Store store = StoreGenerator.getNewPet();

        StoreService storeService = new StoreService();
        Response response = storeService.PlaceAnOrderForAPet(store);
        StoreOut actual = response.then()
                .log().all()
                .assertThat()
                .statusCode(200)
                .extract()
                .body()
                .as(StoreOut.class);

        StoreOut expected = StoreOut.builder()
                .id(88230623L)
                .petId(0L)
                .quantity(0)
                .shipDate("2022-05-13T07:16:50.638+0000")
                .status("placed")
                .complete(true)
                .build();

        Assertions.assertEquals(expected, actual);



    }


    @ParameterizedTest
    @ValueSource(ints = {1})
    public void findPurchaseOrderByIdTest(Integer orderId){
       StoreService storeService = new StoreService();
        Response response = storeService.FindPurchaseOrderById(orderId);
        StoreOut actual = response.then()
                .log().all()
                .statusCode(404)
                .extract()
                .body()
                .as(StoreOut.class);

        StoreOut expected = StoreOut.builder()
                .code(1L)
                .type("error")
                .message("Order not found")
                .build();

        Assertions.assertEquals(expected, actual);
    }


    @ParameterizedTest
    @ValueSource(ints = {1})
    public void deletePurchaseOrderByIdTest(Integer orderId){
        StoreService storeService = new StoreService();
        Response response = storeService.DeletePurchaseOrderById(orderId);
        StoreOut actual = response.then()
                .log().all()
                .statusCode(404)
                .extract()
                .body()
                .as(StoreOut.class);

        StoreOut expected = StoreOut.builder()
                .code(404L)
                .message("Order Not Found")
                .type("unknown")
                .build();

        Assertions.assertEquals(expected, actual);
    }



}




